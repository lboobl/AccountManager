﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALCommon : ALBase
    {
        #region 当前静态实例对象

        static ALCommon _Instance;
        public static ALCommon Instance
        {
            get
            {
                if (_Instance == null)
                {
                    _Instance = new ALCommon();
                }
                return _Instance;
            }
        }

        #endregion

    }
}