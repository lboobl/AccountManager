﻿using System;
using System.Collections.Generic;
using System.Text;
using ZhCun.Framework.Common.Models;
using AccountManager.TableModel.Model;
using AccountManager.Runtime;
using AccountManager.AL.Enums;

namespace AccountManager.AL
{
    public class ALZhangHu : ALBase
    {
        public List<TAccount> GetAccountData(ISearch serObj)
        {
           List<TAccount> rList = DA.GetList<TAccount>(serObj);
           return rList;
        }

        public bool AddAccount(TAccount model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch
                .Add(TAccount.CNActName).Equal(model.ActName)
                .And(TAccount.CNId).Equal(LoginInfo.OperatorId);
            bool r = DA.IsExist<TAccount>(CommonSearch);
            if (r)
            {
                errMsg = "账户名称已存在!";
                return false;
            }
            model.Id = GetGuid();
            model.AddTime = GetNowTime();
            model.LastTime = GetNowTime();
            model.OperatorId = LoginInfo.OperatorId;
            errMsg = string.Empty;

            DA.Add(model);
            return true;
        }

        public bool UpdateAccount(TAccount model, out string errMsg)
        {
            CommonSearch.Clear();
            CommonSearch
                .Add(TAccount.CNId).EqualNot(model.Id)
                .And(TAccount.CNActName).Equal(model.ActName)
                .And(TAccount.CNOperatorId).Equal(LoginInfo.OperatorId);
            bool r = DA.IsExist<TAccount>(CommonSearch);
            if (r)
            {
                errMsg = "账户名称已使用!";
                return false;
            }
            errMsg = string.Empty;
            model.LastTime = GetNowTime();
            DA.Update(model, TAccount.CNActName, TAccount.CNActNamePY, TAccount.CNRemark, TAccount.CNLastTime);
            return true;
        }

        public bool DeleteAccount(TAccount selModel, out string errMsg)
        {
            DA.Delete<TAccount>(selModel.Id);
            errMsg = string.Empty;
            return true;
        }
        
    }
}