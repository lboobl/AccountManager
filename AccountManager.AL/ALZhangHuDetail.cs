﻿using System;
using System.Collections.Generic;
using System.Text;
using AccountManager.TableModel.Model;

namespace AccountManager.AL
{
    public class ALZhangHuDetail : ALBase
    {
        public class ConsumeSearchModel
        {
            public DateTime StartTime { set; get; }
            public DateTime EndTime { set; get; }
            public string Account { set; get; }
            public bool IsAddTime { set; get; }
        }
        public List<VConsume> GetConsumeData(ConsumeSearchModel serModel)
        {
            CommonSearch.Clear();
            string timeColumn = serModel.IsAddTime ? VConsume.CNAddTime : VConsume.CNConsumeDate;
            CommonSearch
                    .And(timeColumn).GreaterThenEqual(serModel.StartTime.ToString("yyyy-MM-dd"))
                    .And(timeColumn).LessThan(serModel.EndTime.AddDays(1).ToString("yyyy-MM-dd"));
            CommonSearch.OrderByDesc(timeColumn);
            if (!string.IsNullOrEmpty(serModel.Account))
            {
                CommonSearch.And(VConsume.CNAccountId).Equal(serModel.Account);
            }
            List<VConsume> rList = DA.GetList<VConsume>(CommonSearch);
            return rList;
        }
    }
}