﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.AL.Enums
{
    public enum EnBalanceType
    {
        /// <summary>
        /// 支出
        /// </summary>
        ZhiChu = 1001,
        /// <summary>
        /// 收入
        /// </summary>
        ShouRu = 1002,
        /// <summary>
        /// 支出冲账
        /// </summary>
        ZhiChuChongZhang = 1003,
        /// <summary>
        /// 收入冲账
        /// </summary>
        ShouRuChongZhang = 1004,
        /// <summary>
        /// 对账支出
        /// </summary>
        DuizhangZhiChu = 1005,
        /// <summary>
        /// 对账收入
        /// </summary>
        DuiZhangShouRu = 1006,
        /// <summary>
        /// 转账支出
        /// </summary>
        ZhuanZhangZhiChu = 1007,
        /// <summary>
        /// 转账收入
        /// </summary>
        ZhuanZhangShouRu = 1008
    }
}