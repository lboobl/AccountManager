﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZhCun.Framework.WinCommon.Forms;
using AccountManager.JiChuZiLiao;
using AccountManager.ChaXunTongJi;
using AccountManager.Runtime;
using AccountManager.AL;
using WeifenLuo.WinFormsUI.Docking;
using AutoUpdateClient;
using System.Diagnostics;
using System.Runtime.InteropServices;
using AccountManager.Notifys;
using System.Threading;
using AccountManager.AppUpdate;

namespace AccountManager
{
    public partial class FrmMDIMain : FrmBase
    {
        public FrmMDIMain()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 在dockPanl中打开单例窗体
        /// </summary>
        protected void ShowMDISubForm<T>() where T : FrmBase, new()
        {
            FrmBase frm = FormsCommon.CreateSingletonForm<T>();
            frm.Show(this.dockPanel1);
        }
        /// <summary>
        /// 设置状态栏信息,及操作权限设置
        /// </summary>
        void SetLoginState()
        {
            tslbLoginTime.Text = LoginInfo.LoginTime.ToString("yyyy-MM-dd HH:mm:ss");
            tslbUserName.Text = LoginInfo.UserName;
            tsMenuOperator.Visible = LoginInfo.OperatorId == "00" ? true : false;
        }
        /// <summary>
        /// 跨线程访问控件时使用,如果不是多个线程内同时访问一个控件不用此方法
        /// </summary>
        protected void ExecThreadMethod(ThreadStart method)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(method);
            }
        }

        #region 鼠标键盘无操作托盘功能

        int _KeyMouseTimeOut;
        int _KeyMouseCheckTime;
        bool _keyMouseCheckStop;

        /// <summary>
        /// 开始检查鼠标键盘无动作
        /// </summary>
        void StartCheckKeyMouse()
        {
            //超时时间
            _KeyMouseTimeOut = AppConfig.KeyMouseTimeOut;
            //检查等待时间
            _KeyMouseCheckTime = 5;
            _keyMouseCheckStop = false;
            Thread th = new Thread(CheckKeyOrMouseMethod);
            th.IsBackground = true;
            th.Start();
        }
        void CheckKeyOrMouseMethod()
        {
            while (true)
            {
                if (_keyMouseCheckStop) break;
                Thread.Sleep(_KeyMouseCheckTime);

                long lastInputTime = WinAPI.GetLastInputTime();
                if (lastInputTime >= _KeyMouseTimeOut * 1000)
                {
                    //Do something
                    //设置窗口最小化
                    //锁定
                    tsBtnLock_Click(this, null);
                }
            }
        }


        #endregion

        #region 检查更新

        /// <summary>
        /// 多长时间检测一次
        /// </summary>
        static int _TimeOut = 5 * 60 * 1000;
        /// <summary>
        /// 是否检查更新
        /// </summary>
        public static bool IsCheckUpdate = true;
        /// <summary>
        /// 开启检查更新的线程
        /// </summary>
        public void CheckUpdateStart()
        {
            Thread th = new Thread(CheckUpdate);
            th.IsBackground = true;
            th.Start();
        }
        void CheckUpdate()
        {
            bool checkResult;
            while (true)
            {
                Thread.Sleep(_TimeOut);
                checkResult = FrmAppUpdate.IsNeedUpdate();
                if (checkResult)
                {
                    ExecThreadMethod(() =>
                    {
                        NotifyHelper.ShowUpdateNotify();
                    });
                }
                if (!IsCheckUpdate)
                {
                    break;
                }
            }
        }

        #endregion

        private void FrmMDIMain_Load(object sender, EventArgs e)
        {
            SetLoginState();
            this.StartPosition = FormStartPosition.CenterScreen;
            //启动程序后可能主程序还没有退出,等待几秒中再替换入口程序
            ThreadPool.QueueUserWorkItem(new WaitCallback(a =>
            {
                Thread.Sleep(2000);
                CheckUpdateStart();

                Thread.Sleep(2000);
                StartCheckKeyMouse();

            }));
        }

        private void tsMenuOperator_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmCaoZuoYuan>();
        }
        private void tsMenuAccount_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZhangHu>();
        }
        private void tsMenuConsumtType_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZhangMuLeiXing>();
        }
        private void tsMenuJiZhang_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmJiZhangView>();
        }
        private void tsMenuDuiZhang_Click(object sender, EventArgs e)
        {
            FrmZhangHuDuiZhang frm = new FrmZhangHuDuiZhang();
            frm.ShowDialog();
        }
        private void tsMenuOutAccount_Click(object sender, EventArgs e)
        {
            FrmZhuangZhang frm = new FrmZhuangZhang();
            frm.ShowDialog();
        }
        private void tsMenuZongHeYeWuMingXi_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZongHeYeWuMingXi>();
        }
        private void tsBtnDuiZhang_Click(object sender, EventArgs e)
        {
            FrmZhangHuDuiZhang frm = new FrmZhangHuDuiZhang();
            frm.ShowDialog();
        }
        private void tsBtnAccount_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZhangHu>();
        }
        private void tsBtnConsumeType_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZhangMuLeiXing>();
        }
        private void tsBtnJiZhang_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmJiZhangView>();
        }
        private void tsLeiXingTongJi_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmLeiXingTongJi>();
        }
        private void tsMenuLeiXingRiTongJi_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmLeiXingRiTongJi>();
        }
        private void tsMenuZhangHuRiTongJi_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZhangHuRiTongJi>();
        }
        private void tsBtnOutAccount_Click(object sender, EventArgs e)
        {
            tsMenuOutAccount_Click(sender, e);
        }
        private void tsMenuZiJinRiTongJi_Click(object sender, EventArgs e)
        {
            ShowMDISubForm<FrmZiJinRiTongJi>();
        }
        private void tsMenuChangeLoginInfo_Click(object sender, EventArgs e)
        {
            FrmUpdateUserInfo frm = new FrmUpdateUserInfo();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                SetLoginState();
            }
        }
        private void tsMenuChangePwd_Click(object sender, EventArgs e)
        {
            FrmUpdatePwd frm = new FrmUpdatePwd();
            frm.ShowDialog();
        }
        private void tsMenuChangeUser_Click(object sender, EventArgs e)
        {
            FrmLogin frm = new FrmLogin();
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                IDockContent[] documents = this.dockPanel1.DocumentsToArray();
                foreach (IDockContent content in documents)
                {
                    content.DockHandler.Close();
                }
                SetLoginState();
            }
        }
        private void tsMenuDataBak_Click(object sender, EventArgs e)
        {
            FrmDataManager frm = new FrmDataManager();
            frm.ShowDialog();
            if (frm.IsRestoreData)
            {
                SetLoginState();
            }
        }
        private void FrmMDIMain_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (!ShowQuestion("确实要退出系统吗?", "退出确认"))
            {
                e.Cancel = true;
            }
        }
        private void tsMenuAbout_Click(object sender, EventArgs e)
        {
            AboutBox ab = new AboutBox();
            ab.ShowDialog();
        }
        private void tsMenuAutoUpdate_Click(object sender, EventArgs e)
        {
            FrmAppUpdate frm = FrmAppUpdate.Create();
            frm.ShowDialog();
        }
        private void tsBtnHelp_Click(object sender, EventArgs e)
        {

        }
        private void tsMenuHelper_Click(object sender, EventArgs e)
        {
            Process.Start("http://www.cnblogs.com/xtdhb/p/AccountManager.html");
        }
        private void tsMenuQQMe_Click(object sender, EventArgs e)
        {
            string url = "http://wpa.qq.com/msgrd?v=3&uin=55500515&site=qq&menu=yes";
            Process.Start(url);
        }
        private void FrmMDIMain_SizeChanged(object sender, EventArgs e)
        {
        }
        private void notifyIcon1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            if (this.ShowInTaskbar == false && notifyIcon1.Visible == true)
            {
                FrmLogin verifyLoginForm = new FrmLogin(LoginInfo.LoginName);
                verifyLoginForm.StartPosition = FormStartPosition.CenterScreen;
                verifyLoginForm.TopMost = true;
                if (verifyLoginForm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {

                    //还原窗体显示 
                    this.WindowState = FormWindowState.Normal;
                    //激活窗体并给予它焦点 
                    this.Activate();
                    //任务栏区显示图标 
                    this.ShowInTaskbar = true;
                    //托盘区图标隐藏 
                    notifyIcon1.Visible = false;

                    //打开一个窗口再关闭,当最小化托盘后再打开,dockpanel的子窗口会显示不正常
                    FrmBase frm = FormsCommon.CreateSingletonForm<FrmLogin>();
                    frm.Show(this.dockPanel1);
                    frm.Close();
                }
            }
        }

        private void tsBtnLock_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
            //隐藏任务栏区图标 
            this.ShowInTaskbar = false;
            //图标显示在托盘区 
            notifyIcon1.Visible = true;
        }
    }
}