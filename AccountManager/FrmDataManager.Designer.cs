﻿namespace AccountManager
{
    partial class FrmDataManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDataBak = new System.Windows.Forms.Button();
            this.btnDataRestore = new System.Windows.Forms.Button();
            this.btnClearConsume = new System.Windows.Forms.Button();
            this.btnClearAll = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnDataBak
            // 
            this.btnDataBak.Location = new System.Drawing.Point(78, 31);
            this.btnDataBak.Name = "btnDataBak";
            this.btnDataBak.Size = new System.Drawing.Size(188, 28);
            this.btnDataBak.TabIndex = 3;
            this.btnDataBak.Text = "数据备份";
            this.btnDataBak.UseVisualStyleBackColor = true;
            this.btnDataBak.Click += new System.EventHandler(this.btnDataBak_Click);
            // 
            // btnDataRestore
            // 
            this.btnDataRestore.Location = new System.Drawing.Point(78, 73);
            this.btnDataRestore.Name = "btnDataRestore";
            this.btnDataRestore.Size = new System.Drawing.Size(188, 28);
            this.btnDataRestore.TabIndex = 3;
            this.btnDataRestore.Text = "数据恢复";
            this.btnDataRestore.UseVisualStyleBackColor = true;
            this.btnDataRestore.Click += new System.EventHandler(this.btnDataRestore_Click);
            // 
            // btnClearConsume
            // 
            this.btnClearConsume.Location = new System.Drawing.Point(78, 116);
            this.btnClearConsume.Name = "btnClearConsume";
            this.btnClearConsume.Size = new System.Drawing.Size(188, 28);
            this.btnClearConsume.TabIndex = 3;
            this.btnClearConsume.Text = "清除账目流水";
            this.btnClearConsume.UseVisualStyleBackColor = true;
            this.btnClearConsume.Click += new System.EventHandler(this.btnClearConsume_Click);
            // 
            // btnClearAll
            // 
            this.btnClearAll.Location = new System.Drawing.Point(78, 159);
            this.btnClearAll.Name = "btnClearAll";
            this.btnClearAll.Size = new System.Drawing.Size(188, 28);
            this.btnClearAll.TabIndex = 3;
            this.btnClearAll.Text = "清除所有数据";
            this.btnClearAll.UseVisualStyleBackColor = true;
            this.btnClearAll.Click += new System.EventHandler(this.btnClearAll_Click);
            // 
            // FrmDataManager
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.ClientSize = new System.Drawing.Size(342, 230);
            this.Controls.Add(this.btnClearAll);
            this.Controls.Add(this.btnClearConsume);
            this.Controls.Add(this.btnDataRestore);
            this.Controls.Add(this.btnDataBak);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmDataManager";
            this.Text = "数据维护";
            this.Load += new System.EventHandler(this.FrmDataManager_Load);
            this.Controls.SetChildIndex(this.btnDataBak, 0);
            this.Controls.SetChildIndex(this.btnDataRestore, 0);
            this.Controls.SetChildIndex(this.btnClearConsume, 0);
            this.Controls.SetChildIndex(this.btnClearAll, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDataBak;
        private System.Windows.Forms.Button btnDataRestore;
        private System.Windows.Forms.Button btnClearConsume;
        private System.Windows.Forms.Button btnClearAll;
    }
}