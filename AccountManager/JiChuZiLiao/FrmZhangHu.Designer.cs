﻿namespace AccountManager.JiChuZiLiao
{
    partial class FrmZhangHu
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolStripTop2 = new System.Windows.Forms.ToolStrip();
            this.tsTxtQuickSearch = new System.Windows.Forms.ToolStripTextBox();
            this.tsBtnQuickSearch = new System.Windows.Forms.ToolStripButton();
            this.tsBtnQuickSearchClear = new System.Windows.Forms.ToolStripButton();
            this.toolStripTop1 = new System.Windows.Forms.ToolStrip();
            this.tsBtnAdd = new System.Windows.Forms.ToolStripButton();
            this.tsBtnEditor = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDelete = new System.Windows.Forms.ToolStripButton();
            this.tsbtnViewDetail = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.tsBtnClose = new System.Windows.Forms.ToolStripButton();
            this.tsBtnDuiZhang = new System.Windows.Forms.ToolStripButton();
            this.tsBtnZhuanZhang = new System.Windows.Forms.ToolStripButton();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbSumAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.ucPagingNavigation = new ZhCun.Framework.WinCommon.Controls.UCPagingNavigation();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.col_ActName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ActNamePY = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Balance = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_LastTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_AddTime = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_Id = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.toolStripTop2.SuspendLayout();
            this.toolStripTop1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // toolStripTop2
            // 
            this.toolStripTop2.AutoSize = false;
            this.toolStripTop2.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsTxtQuickSearch,
            this.tsBtnQuickSearch,
            this.tsBtnQuickSearchClear});
            this.toolStripTop2.Location = new System.Drawing.Point(0, 38);
            this.toolStripTop2.Name = "toolStripTop2";
            this.toolStripTop2.Size = new System.Drawing.Size(828, 35);
            this.toolStripTop2.TabIndex = 18;
            this.toolStripTop2.Text = "toolStrip3";
            // 
            // tsTxtQuickSearch
            // 
            this.tsTxtQuickSearch.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsTxtQuickSearch.Margin = new System.Windows.Forms.Padding(5);
            this.tsTxtQuickSearch.Name = "tsTxtQuickSearch";
            this.tsTxtQuickSearch.Size = new System.Drawing.Size(148, 25);
            this.tsTxtQuickSearch.ToolTipText = "输入回车进行快速检索";
            this.tsTxtQuickSearch.KeyDown += new System.Windows.Forms.KeyEventHandler(this.tsTxtQuickSearch_KeyDown);
            // 
            // tsBtnQuickSearch
            // 
            this.tsBtnQuickSearch.Checked = true;
            this.tsBtnQuickSearch.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnQuickSearch.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnQuickSearch.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnQuickSearch.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnQuickSearch.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnQuickSearch.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnQuickSearch.Name = "tsBtnQuickSearch";
            this.tsBtnQuickSearch.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnQuickSearch.Size = new System.Drawing.Size(61, 32);
            this.tsBtnQuickSearch.Text = "检索";
            this.tsBtnQuickSearch.Click += new System.EventHandler(this.tsBtnQuickSearch_Click);
            // 
            // tsBtnQuickSearchClear
            // 
            this.tsBtnQuickSearchClear.Checked = true;
            this.tsBtnQuickSearchClear.CheckState = System.Windows.Forms.CheckState.Checked;
            this.tsBtnQuickSearchClear.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.tsBtnQuickSearchClear.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnQuickSearchClear.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnQuickSearchClear.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnQuickSearchClear.Margin = new System.Windows.Forms.Padding(0, 1, 5, 2);
            this.tsBtnQuickSearchClear.Name = "tsBtnQuickSearchClear";
            this.tsBtnQuickSearchClear.Padding = new System.Windows.Forms.Padding(0, 0, 20, 0);
            this.tsBtnQuickSearchClear.Size = new System.Drawing.Size(61, 32);
            this.tsBtnQuickSearchClear.Text = "清空";
            this.tsBtnQuickSearchClear.Click += new System.EventHandler(this.tsBtnQuickSearchClear_Click);
            // 
            // toolStripTop1
            // 
            this.toolStripTop1.AutoSize = false;
            this.toolStripTop1.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.toolStripTop1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsBtnAdd,
            this.tsBtnEditor,
            this.tsBtnDelete,
            this.tsbtnViewDetail,
            this.toolStripSeparator5,
            this.tsBtnClose,
            this.tsBtnDuiZhang,
            this.tsBtnZhuanZhang});
            this.toolStripTop1.Location = new System.Drawing.Point(0, 0);
            this.toolStripTop1.Name = "toolStripTop1";
            this.toolStripTop1.Size = new System.Drawing.Size(828, 38);
            this.toolStripTop1.TabIndex = 17;
            this.toolStripTop1.Text = "toolStrip2";
            // 
            // tsBtnAdd
            // 
            this.tsBtnAdd.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnAdd.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnAdd.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnAdd.Image = global::AccountManager.Properties.Resources.add;
            this.tsBtnAdd.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnAdd.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnAdd.Name = "tsBtnAdd";
            this.tsBtnAdd.Size = new System.Drawing.Size(73, 35);
            this.tsBtnAdd.Text = "增加";
            this.tsBtnAdd.Click += new System.EventHandler(this.tsBtnAdd_Click);
            // 
            // tsBtnEditor
            // 
            this.tsBtnEditor.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnEditor.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnEditor.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnEditor.Image = global::AccountManager.Properties.Resources.edit;
            this.tsBtnEditor.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnEditor.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnEditor.Name = "tsBtnEditor";
            this.tsBtnEditor.Size = new System.Drawing.Size(73, 35);
            this.tsBtnEditor.Text = "编辑";
            this.tsBtnEditor.Click += new System.EventHandler(this.tsBtnEditor_Click);
            // 
            // tsBtnDelete
            // 
            this.tsBtnDelete.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnDelete.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnDelete.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnDelete.Image = global::AccountManager.Properties.Resources.delete;
            this.tsBtnDelete.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnDelete.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDelete.Name = "tsBtnDelete";
            this.tsBtnDelete.Size = new System.Drawing.Size(73, 35);
            this.tsBtnDelete.Text = "删除";
            this.tsBtnDelete.Click += new System.EventHandler(this.tsBtnDelete_Click);
            // 
            // tsbtnViewDetail
            // 
            this.tsbtnViewDetail.BackColor = System.Drawing.Color.Transparent;
            this.tsbtnViewDetail.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsbtnViewDetail.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsbtnViewDetail.Image = global::AccountManager.Properties.Resources.viewdetail;
            this.tsbtnViewDetail.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsbtnViewDetail.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsbtnViewDetail.Name = "tsbtnViewDetail";
            this.tsbtnViewDetail.Size = new System.Drawing.Size(73, 35);
            this.tsbtnViewDetail.Text = "明细";
            this.tsbtnViewDetail.Click += new System.EventHandler(this.tsbtnViewDetail_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 38);
            // 
            // tsBtnClose
            // 
            this.tsBtnClose.Alignment = System.Windows.Forms.ToolStripItemAlignment.Right;
            this.tsBtnClose.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnClose.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnClose.Image = global::AccountManager.Properties.Resources.exit;
            this.tsBtnClose.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnClose.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnClose.Margin = new System.Windows.Forms.Padding(0, 1, 1, 2);
            this.tsBtnClose.Name = "tsBtnClose";
            this.tsBtnClose.Size = new System.Drawing.Size(73, 35);
            this.tsBtnClose.Text = "退出";
            this.tsBtnClose.Click += new System.EventHandler(this.tsBtnClose_Click);
            // 
            // tsBtnDuiZhang
            // 
            this.tsBtnDuiZhang.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnDuiZhang.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnDuiZhang.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnDuiZhang.Image = global::AccountManager.Properties.Resources.book;
            this.tsBtnDuiZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnDuiZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnDuiZhang.Name = "tsBtnDuiZhang";
            this.tsBtnDuiZhang.Size = new System.Drawing.Size(73, 35);
            this.tsBtnDuiZhang.Text = "对账";
            this.tsBtnDuiZhang.Click += new System.EventHandler(this.tsBtnDuiZhang_Click);
            // 
            // tsBtnZhuanZhang
            // 
            this.tsBtnZhuanZhang.BackColor = System.Drawing.Color.Transparent;
            this.tsBtnZhuanZhang.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.tsBtnZhuanZhang.ForeColor = System.Drawing.SystemColors.HotTrack;
            this.tsBtnZhuanZhang.Image = global::AccountManager.Properties.Resources.account;
            this.tsBtnZhuanZhang.ImageScaling = System.Windows.Forms.ToolStripItemImageScaling.None;
            this.tsBtnZhuanZhang.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.tsBtnZhuanZhang.Name = "tsBtnZhuanZhang";
            this.tsBtnZhuanZhang.Size = new System.Drawing.Size(73, 35);
            this.tsBtnZhuanZhang.Text = "转账";
            this.tsBtnZhuanZhang.Click += new System.EventHandler(this.tsBtnZhuanZhang_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel3,
            this.tslbSumAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 538);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(828, 22);
            this.statusStrip1.TabIndex = 27;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel3.Text = "账户总余额：";
            // 
            // tslbSumAmount
            // 
            this.tslbSumAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbSumAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbSumAmount.Name = "tslbSumAmount";
            this.tslbSumAmount.Size = new System.Drawing.Size(20, 17);
            this.tslbSumAmount.Text = "０";
            // 
            // ucPagingNavigation
            // 
            this.ucPagingNavigation.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.ucPagingNavigation.Location = new System.Drawing.Point(0, 502);
            this.ucPagingNavigation.Name = "ucPagingNavigation";
            this.ucPagingNavigation.Size = new System.Drawing.Size(828, 36);
            this.ucPagingNavigation.TabIndex = 28;
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ActName,
            this.col_ActNamePY,
            this.col_Balance,
            this.col_LastTime,
            this.col_AddTime,
            this.col_Id});
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dgv.DefaultCellStyle = dataGridViewCellStyle3;
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 73);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgv.RowHeadersDefaultCellStyle = dataGridViewCellStyle4;
            dataGridViewCellStyle5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle5;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(828, 429);
            this.dgv.TabIndex = 29;
            this.dgv.UseControlStyle = true;
            // 
            // col_ActName
            // 
            this.col_ActName.DataPropertyName = "ActName";
            this.col_ActName.HeaderText = "账户名称";
            this.col_ActName.Name = "col_ActName";
            this.col_ActName.ReadOnly = true;
            // 
            // col_ActNamePY
            // 
            this.col_ActNamePY.DataPropertyName = "ActNamePY";
            this.col_ActNamePY.HeaderText = "助记码";
            this.col_ActNamePY.Name = "col_ActNamePY";
            this.col_ActNamePY.ReadOnly = true;
            // 
            // col_Balance
            // 
            this.col_Balance.DataPropertyName = "Balance";
            this.col_Balance.HeaderText = "余额";
            this.col_Balance.Name = "col_Balance";
            this.col_Balance.ReadOnly = true;
            // 
            // col_LastTime
            // 
            this.col_LastTime.DataPropertyName = "LastTime";
            this.col_LastTime.HeaderText = "更新时间";
            this.col_LastTime.Name = "col_LastTime";
            this.col_LastTime.ReadOnly = true;
            // 
            // col_AddTime
            // 
            this.col_AddTime.DataPropertyName = "AddTime";
            this.col_AddTime.HeaderText = "增加时间";
            this.col_AddTime.Name = "col_AddTime";
            this.col_AddTime.ReadOnly = true;
            // 
            // col_Id
            // 
            this.col_Id.DataPropertyName = "Id";
            this.col_Id.HeaderText = "Id";
            this.col_Id.Name = "col_Id";
            this.col_Id.ReadOnly = true;
            this.col_Id.Visible = false;
            // 
            // FrmZhangHu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(828, 560);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.ucPagingNavigation);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.toolStripTop2);
            this.Controls.Add(this.toolStripTop1);
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FrmZhangHu";
            this.Text = "账户信息";
            this.Load += new System.EventHandler(this.FrmZhangHu_Load);
            this.Controls.SetChildIndex(this.toolStripTop1, 0);
            this.Controls.SetChildIndex(this.toolStripTop2, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.ucPagingNavigation, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.toolStripTop2.ResumeLayout(false);
            this.toolStripTop2.PerformLayout();
            this.toolStripTop1.ResumeLayout(false);
            this.toolStripTop1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.ToolStrip toolStripTop2;
        public System.Windows.Forms.ToolStripTextBox tsTxtQuickSearch;
        public System.Windows.Forms.ToolStripButton tsBtnQuickSearch;
        public System.Windows.Forms.ToolStripButton tsBtnQuickSearchClear;
        public System.Windows.Forms.ToolStrip toolStripTop1;
        public System.Windows.Forms.ToolStripButton tsBtnAdd;
        public System.Windows.Forms.ToolStripButton tsBtnEditor;
        public System.Windows.Forms.ToolStripButton tsBtnDelete;
        public System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        public System.Windows.Forms.ToolStripButton tsBtnClose;
        public System.Windows.Forms.ToolStripButton tsBtnDuiZhang;
        public System.Windows.Forms.ToolStripButton tsBtnZhuanZhang;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tslbSumAmount;
        private ZhCun.Framework.WinCommon.Controls.UCPagingNavigation ucPagingNavigation;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ActName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ActNamePY;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Balance;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_LastTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_AddTime;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_Id;
        public System.Windows.Forms.ToolStripButton tsbtnViewDetail;
    }
}