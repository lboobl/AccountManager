﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using ZhCun.Framework.WinCommon.Forms;
using AccountManager.AL;
using ZhCun.Framework.Common.Models;
using AccountManager.TableModel.Model;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmCaoZuoYuan : BaseForm
    {
        public FrmCaoZuoYuan()
        {
            InitializeComponent();
        }

        ALCaoZuoYuan _ALObj;
        ISearch _SearchObj;

        void BindDataSource()
        {
            _SearchObj.Clear();
            if (tsTxtQuickSearch.Text != string.Empty)
            {
                _SearchObj
                    .Add(TOperator.CNName).LikeFull(tsTxtQuickSearch.Text)
                    .Or(TOperator.CNNamePY).LikeFull(tsTxtQuickSearch.Text);

            }
            _SearchObj.OnePage = ucPagingNavigation.OnePageCount;
            _SearchObj.PageNo = 1;
            dgv.DataSource = _ALObj.GetOperatorList(_SearchObj);
            ucPagingNavigation.InitiControl(_SearchObj.RecordCount);
        }

        private void tsBtnAdd_Click(object sender, EventArgs e)
        {
            FrmCaoZuoYuanEdit frm = new FrmCaoZuoYuanEdit(_ALObj.AddOperator);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void FrmCaoZuoYuan_Load(object sender, EventArgs e)
        {
            _ALObj = new ALCaoZuoYuan();
            _SearchObj = _ALObj.CreateSearch();
            BindDataSource();
        }

        private void tsBtnQuickSearch_Click(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void tsBtnEditor_Click(object sender, EventArgs e)
        {
            TOperator selModel = dgv.GetSelectedClassData<TOperator>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何行记录");
                return;
            }
            FrmCaoZuoYuanEdit frm = new FrmCaoZuoYuanEdit(_ALObj.UpdateOperator, selModel);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            TOperator selModel = dgv.GetSelectedClassData<TOperator>();
            if (selModel == null)
            {
                ShowMessage("没有选中任何行记录");
                return;
            }
            if (!ShowQuestion("确实要删除指定记录吗?", "删除确认"))
            {
                return;
            }
            string errMsg;
            bool delResult = _ALObj.DeleteOperator(selModel, out errMsg);
            if (delResult)
            {
                BindDataSource();
                ShowMessage("删除成功!");
            }
            else
            {
                ShowMessage("删除失败!\r\n{0}", errMsg);
            }
        }

        private void tsBtnQuickSearchClear_Click(object sender, EventArgs e)
        {
            tsTxtQuickSearch.Text = string.Empty;
        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsTxtQuickSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BindDataSource();
            }
        }

        private void ucPagingNavigation_PageChangedEvent(int pageNo, int onePageCount, out int recordCount)
        {
            _SearchObj.OnePage = onePageCount;
            _SearchObj.PageNo = pageNo;
            dgv.DataSource = _ALObj.GetOperatorList(_SearchObj);
            recordCount = _SearchObj.RecordCount;
        }
    }
}