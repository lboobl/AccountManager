﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;
using AccountManager.TableModel.Model;
using ZhCun.Framework.Common.Models;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangMuLeiXing : BaseForm
    {
        public FrmZhangMuLeiXing()
        {
            InitializeComponent();
        }

        ALZhangMuLeiXing _ALObj;
        string _SearchValue;

        void BindDataSource()
        {
            _SearchValue = tsTxtQuickSearch.Text;
            int recordCount;
            List<VConsumeType> rList = _ALObj.GetConsumeTypeData(_SearchValue, 1, ucPagingNavigation.OnePageCount, out recordCount);
            ucPagingNavigation.InitiControl(recordCount);
            dgv.DataSource = rList;
        }

        private void FrmZhangMuLeiXing_new_Load(object sender, EventArgs e)
        {
            _ALObj = new ALZhangMuLeiXing();
            BindDataSource();
        }

        private void tsBtnAdd_Click(object sender, EventArgs e)
        {
            FrmZhangMuLeiXingEdit frm = new FrmZhangMuLeiXingEdit(_ALObj.AddConsumeType);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void tsBtnQuickSearch_Click(object sender, EventArgs e)
        {
            BindDataSource();
        }

        private void tsTxtQuickSearch_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                BindDataSource();
            }
        }

        private void tsBtnQuickSearchClear_Click(object sender, EventArgs e)
        {
            tsTxtQuickSearch.Clear();
        }

        private void tsBtnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tsBtnEditor_Click(object sender, EventArgs e)
        {
            VConsumeType selModel = dgv.GetSelectedClassData<VConsumeType>();
            if (selModel == null)
            {
                ShowMessage("请选中要编辑的记录");
                return;
            }
            FrmZhangMuLeiXingEdit frm = new FrmZhangMuLeiXingEdit(_ALObj.UpdateConsumeType, selModel);
            if (frm.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                BindDataSource();
            }
        }

        private void tsBtnDelete_Click(object sender, EventArgs e)
        {
            VConsumeType selModel = dgv.GetSelectedClassData<VConsumeType>();
            if (selModel == null)
            {
                ShowMessage("请选中要删除的记录");
                return;
            }
            if (ShowQuestion("确实要删除当前选中的记录吗?", "删除确认"))
            {
                string errMsg;
                bool r = _ALObj.DeleteConsumeType(selModel, out errMsg);
                if (!r)
                {
                    ShowMessage(errMsg);
                }
                else
                {
                    BindDataSource();
                }
            }
        }

        private void ucPagingNavigation_PageChangedEvent(int pageNo, int onePageCount, out int recordCount)
        {
             dgv.DataSource = _ALObj.GetConsumeTypeData(_SearchValue, pageNo, onePageCount, out recordCount);
        }
    }
}