﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.TableModel.Model;
using ZhCun.Framework.Common.Helpers;
using AccountManager.AL;

namespace AccountManager.JiChuZiLiao
{
    public partial class FrmZhangHuEdit : BaseForm
    {
        public FrmZhangHuEdit(EditFormHandle<TAccount> editHandle)
            : this(editHandle, null)
        { }

        public FrmZhangHuEdit(EditFormHandle<TAccount> editHandle, TAccount model)
        {
            InitializeComponent();
            _Model = model;
            this.EditHandle = editHandle;
        }

        TAccount _Model;

        EditFormHandle<TAccount> EditHandle;

        public TAccount AccountModel
        {
            get
            {
                return _Model;
            }
        }

        private void FrmZhangHuEdit_Load(object sender, EventArgs e)
        {
            if (_Model == null)
            {
                _Model = new TAccount();
            }
            else
            {
                dcm1.SetControlValue(_Model);
            }
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (txtActName.Text.Trim().Length == 0)
            {
                ShowMessage("账户名称不能为空!");
                return;
            }
            dcm1.SetValueToClassObj(_Model);
            string errMsg;
            bool r = EditHandle(_Model, out errMsg);
            if (r)
            {
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                ShowMessage(errMsg);
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void txtActName_Leave(object sender, EventArgs e)
        {
            string py = StringHelper.GetPinYin(txtActName.Text, true);
            txtActNamePY.Text = py;
        }
    }
}