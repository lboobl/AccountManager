﻿namespace AccountManager.ChaXunTongJi
{
    partial class FrmLeiXingTongJi
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.btnClear = new System.Windows.Forms.Button();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtAccount = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.txtConsumeType = new ZhCun.Framework.WinCommon.Controls.ZCTextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.chBoxIsAddTime = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbRecordCount = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel2 = new System.Windows.Forms.ToolStripStatusLabel();
            this.toolStripStatusLabel3 = new System.Windows.Forms.ToolStripStatusLabel();
            this.tslbSumAmount = new System.Windows.Forms.ToolStripStatusLabel();
            this.dgv = new ZhCun.Framework.WinCommon.Controls.ZCDataGridView();
            this.dcm1 = new ZhCun.Framework.WinCommon.Components.DCM(this.components);
            this.comboxList1 = new ZhCun.Framework.WinCommon.Components.ComboxList(this.components);
            this.col_ConsumeTypeName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_SumAmount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_UseCount = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.col_ConsumeTypeId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.groupBox1.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.btnClear);
            this.groupBox1.Controls.Add(this.btnSearch);
            this.groupBox1.Controls.Add(this.txtAccount);
            this.groupBox1.Controls.Add(this.txtConsumeType);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.chBoxIsAddTime);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.dtpStartDate);
            this.groupBox1.Controls.Add(this.dtpEndDate);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.groupBox1.Location = new System.Drawing.Point(0, 0);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(973, 100);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "查询条件";
            // 
            // btnClear
            // 
            this.btnClear.Location = new System.Drawing.Point(695, 37);
            this.btnClear.Name = "btnClear";
            this.btnClear.Size = new System.Drawing.Size(75, 30);
            this.btnClear.TabIndex = 24;
            this.btnClear.Text = "清空";
            this.btnClear.UseVisualStyleBackColor = true;
            this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
            // 
            // btnSearch
            // 
            this.btnSearch.Location = new System.Drawing.Point(582, 37);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(75, 30);
            this.btnSearch.TabIndex = 24;
            this.btnSearch.Text = "查询";
            this.btnSearch.UseVisualStyleBackColor = true;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtAccount
            // 
            this.comboxList1.SetColumns(this.txtAccount, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtAccount, 0);
            this.txtAccount.EmptyTextTip = "回车选择账户";
            this.txtAccount.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtAccount, 0);
            this.dcm1.SetIsUse(this.txtAccount, true);
            this.comboxList1.SetIsUse(this.txtAccount, false);
            this.txtAccount.Location = new System.Drawing.Point(358, 60);
            this.txtAccount.Name = "txtAccount";
            this.comboxList1.SetNextControl(this.txtAccount, this.txtAccount);
            this.txtAccount.Size = new System.Drawing.Size(152, 21);
            this.txtAccount.TabIndex = 23;
            this.dcm1.SetTagColumnName(this.txtAccount, "AccountId");
            this.dcm1.SetTextColumnName(this.txtAccount, null);
            // 
            // txtConsumeType
            // 
            this.comboxList1.SetColumns(this.txtConsumeType, new ZhCun.Framework.WinCommon.Components.ComBoxListColumn[0]);
            this.comboxList1.SetDisplayRowCount(this.txtConsumeType, 0);
            this.txtConsumeType.EmptyTextTip = "回车选择账目类型";
            this.txtConsumeType.EmptyTextTipColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(192)))), ((int)(((byte)(255)))));
            this.comboxList1.SetGridViewWidth(this.txtConsumeType, 0);
            this.dcm1.SetIsUse(this.txtConsumeType, true);
            this.comboxList1.SetIsUse(this.txtConsumeType, false);
            this.txtConsumeType.Location = new System.Drawing.Point(358, 29);
            this.txtConsumeType.Name = "txtConsumeType";
            this.comboxList1.SetNextControl(this.txtConsumeType, this.txtConsumeType);
            this.txtConsumeType.Size = new System.Drawing.Size(152, 21);
            this.txtConsumeType.TabIndex = 22;
            this.dcm1.SetTagColumnName(this.txtConsumeType, "ConsumeTypeId");
            this.dcm1.SetTextColumnName(this.txtConsumeType, null);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(316, 60);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 12);
            this.label1.TabIndex = 21;
            this.label1.Text = "账户:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(292, 32);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 12);
            this.label2.TabIndex = 21;
            this.label2.Text = "账目类型:";
            // 
            // chBoxIsAddTime
            // 
            this.chBoxIsAddTime.Checked = true;
            this.chBoxIsAddTime.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dcm1.SetIsUse(this.chBoxIsAddTime, true);
            this.chBoxIsAddTime.Location = new System.Drawing.Point(221, 32);
            this.chBoxIsAddTime.Name = "chBoxIsAddTime";
            this.chBoxIsAddTime.Size = new System.Drawing.Size(56, 49);
            this.chBoxIsAddTime.TabIndex = 19;
            this.dcm1.SetTagColumnName(this.chBoxIsAddTime, null);
            this.chBoxIsAddTime.Text = "记账时间";
            this.dcm1.SetTextColumnName(this.chBoxIsAddTime, "IsAddTime");
            this.chBoxIsAddTime.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(19, 33);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 12);
            this.label3.TabIndex = 15;
            this.label3.Text = "开始日期:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(18, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(59, 12);
            this.label4.TabIndex = 16;
            this.label4.Text = "结束日期:";
            // 
            // dtpStartDate
            // 
            this.dcm1.SetIsUse(this.dtpStartDate, true);
            this.dtpStartDate.Location = new System.Drawing.Point(82, 29);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(133, 21);
            this.dtpStartDate.TabIndex = 17;
            this.dcm1.SetTagColumnName(this.dtpStartDate, null);
            this.dcm1.SetTextColumnName(this.dtpStartDate, "StartDate");
            // 
            // dtpEndDate
            // 
            this.dcm1.SetIsUse(this.dtpEndDate, true);
            this.dtpEndDate.Location = new System.Drawing.Point(82, 60);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(133, 21);
            this.dtpEndDate.TabIndex = 18;
            this.dcm1.SetTagColumnName(this.dtpEndDate, null);
            this.dcm1.SetTextColumnName(this.dtpEndDate, "EndDate");
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1,
            this.tslbRecordCount,
            this.toolStripStatusLabel2,
            this.toolStripStatusLabel3,
            this.tslbSumAmount});
            this.statusStrip1.Location = new System.Drawing.Point(0, 554);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(973, 22);
            this.statusStrip1.TabIndex = 26;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(56, 17);
            this.toolStripStatusLabel1.Text = "记录数：";
            // 
            // tslbRecordCount
            // 
            this.tslbRecordCount.BackColor = System.Drawing.Color.Transparent;
            this.tslbRecordCount.ForeColor = System.Drawing.Color.Red;
            this.tslbRecordCount.Name = "tslbRecordCount";
            this.tslbRecordCount.Size = new System.Drawing.Size(20, 17);
            this.tslbRecordCount.Text = "０";
            // 
            // toolStripStatusLabel2
            // 
            this.toolStripStatusLabel2.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel2.Name = "toolStripStatusLabel2";
            this.toolStripStatusLabel2.Size = new System.Drawing.Size(11, 17);
            this.toolStripStatusLabel2.Text = "|";
            // 
            // toolStripStatusLabel3
            // 
            this.toolStripStatusLabel3.BackColor = System.Drawing.Color.Transparent;
            this.toolStripStatusLabel3.Name = "toolStripStatusLabel3";
            this.toolStripStatusLabel3.Size = new System.Drawing.Size(80, 17);
            this.toolStripStatusLabel3.Text = "账户增加额：";
            // 
            // tslbSumAmount
            // 
            this.tslbSumAmount.BackColor = System.Drawing.Color.Transparent;
            this.tslbSumAmount.ForeColor = System.Drawing.Color.Red;
            this.tslbSumAmount.Name = "tslbSumAmount";
            this.tslbSumAmount.Size = new System.Drawing.Size(20, 17);
            this.tslbSumAmount.Text = "０";
            // 
            // dgv
            // 
            this.dgv.AllowUserToAddRows = false;
            this.dgv.AllowUserToDeleteRows = false;
            this.dgv.AllowUserToResizeRows = false;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            this.dgv.AlternatingRowsDefaultCellStyle = dataGridViewCellStyle1;
            this.dgv.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgv.BackgroundColor = System.Drawing.Color.Snow;
            this.dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgv.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.col_ConsumeTypeName,
            this.col_SumAmount,
            this.col_UseCount,
            this.col_ConsumeTypeId});
            this.dgv.DisplayRowCount = true;
            this.dgv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgv.Location = new System.Drawing.Point(0, 100);
            this.dgv.MultiSelect = false;
            this.dgv.Name = "dgv";
            this.dgv.ReadOnly = true;
            dataGridViewCellStyle2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dgv.RowsDefaultCellStyle = dataGridViewCellStyle2;
            this.dgv.RowTemplate.Height = 23;
            this.dgv.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgv.Size = new System.Drawing.Size(973, 454);
            this.dgv.TabIndex = 27;
            this.dgv.UseControlStyle = true;
            // 
            // comboxList1
            // 
            this.comboxList1.GridViewDataSource = null;
            this.comboxList1.MaxRowCount = 5;
            // 
            // col_ConsumeTypeName
            // 
            this.col_ConsumeTypeName.DataPropertyName = "ConsumeTypeName";
            this.col_ConsumeTypeName.HeaderText = "账目类型";
            this.col_ConsumeTypeName.Name = "col_ConsumeTypeName";
            this.col_ConsumeTypeName.ReadOnly = true;
            // 
            // col_SumAmount
            // 
            this.col_SumAmount.DataPropertyName = "SumAmount";
            this.col_SumAmount.HeaderText = "增加额";
            this.col_SumAmount.Name = "col_SumAmount";
            this.col_SumAmount.ReadOnly = true;
            // 
            // col_UseCount
            // 
            this.col_UseCount.DataPropertyName = "UseCount";
            this.col_UseCount.HeaderText = "类型使用次数";
            this.col_UseCount.Name = "col_UseCount";
            this.col_UseCount.ReadOnly = true;
            // 
            // col_ConsumeTypeId
            // 
            this.col_ConsumeTypeId.DataPropertyName = "ConsumeTypeId";
            this.col_ConsumeTypeId.HeaderText = "ConsumeTypeId";
            this.col_ConsumeTypeId.Name = "col_ConsumeTypeId";
            this.col_ConsumeTypeId.ReadOnly = true;
            this.col_ConsumeTypeId.Visible = false;
            // 
            // FrmLeiXingTongJi
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(973, 576);
            this.Controls.Add(this.dgv);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.groupBox1);
            this.Font = new System.Drawing.Font("宋体", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.Name = "FrmLeiXingTongJi";
            this.Text = "账目类型统计";
            this.Load += new System.EventHandler(this.FrmLeiXingTongJi_Load);
            this.Controls.SetChildIndex(this.groupBox1, 0);
            this.Controls.SetChildIndex(this.statusStrip1, 0);
            this.Controls.SetChildIndex(this.dgv, 0);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgv)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ToolStripStatusLabel tslbRecordCount;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel2;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel3;
        private System.Windows.Forms.ToolStripStatusLabel tslbSumAmount;
        private System.Windows.Forms.CheckBox chBoxIsAddTime;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private ZhCun.Framework.WinCommon.Controls.ZCDataGridView dgv;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtConsumeType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private ZhCun.Framework.WinCommon.Controls.ZCTextBox txtAccount;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.Button btnClear;
        private ZhCun.Framework.WinCommon.Components.DCM dcm1;
        private ZhCun.Framework.WinCommon.Components.ComboxList comboxList1;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeTypeName;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_SumAmount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_UseCount;
        private System.Windows.Forms.DataGridViewTextBoxColumn col_ConsumeTypeId;
    }
}