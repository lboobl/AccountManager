﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AccountManager.Notifys
{
    public class FrmNormalNotify : FrmNotify
    {
        public FrmNormalNotify()
        {
            InitializeComponent();
        }
        public System.Windows.Forms.Label lbCentent;

        private void InitializeComponent()
        {
            this.lbCentent = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // lbCentent
            // 
            this.lbCentent.Font = new System.Drawing.Font("微软雅黑", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.lbCentent.Location = new System.Drawing.Point(43, 54);
            this.lbCentent.Name = "lbCentent";
            this.lbCentent.Size = new System.Drawing.Size(226, 141);
            this.lbCentent.TabIndex = 1;
            this.lbCentent.Text = "通知内容";
            // 
            // FrmNormalNotify
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.ClientSize = new System.Drawing.Size(307, 253);
            this.Controls.Add(this.lbCentent);
            this.Location = new System.Drawing.Point(0, 0);
            this.Name = "FrmNormalNotify";
            this.Controls.SetChildIndex(this.lbCentent, 0);
            this.ResumeLayout(false);
            this.PerformLayout();

        }
    }
}
