﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using AccountManager.AL;

namespace AccountManager
{
    public partial class FrmUpdatePwd : BaseForm
    {
        public FrmUpdatePwd()
        {
            InitializeComponent();
        }
        ALLogin _ALObj;

        private void btnOK_Click(object sender, EventArgs e)
        {
            string errMsg;
            bool r = _ALObj.ChangePassword(txtOldPwd.Text, txtNewPwd.Text, txtRNewPwd.Text, out errMsg);
            if (r)
            {
                MessageBox.Show("修改密码成功!");
                this.DialogResult = System.Windows.Forms.DialogResult.OK;
            }
            else
            {
                ShowMessage(errMsg);
            }
        }

        private void FrmUpdatePwd_Load(object sender, EventArgs e)
        {
            _ALObj = new ALLogin();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
