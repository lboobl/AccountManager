﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using System.Diagnostics;
using System.IO;
using System.Reflection;
using System.Threading;

namespace AccountManager.Run
{
    static class Program
    {
        /// <summary>
        /// 应用程序的主入口点。
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            if (Directory.Exists(UpdatePath) && Directory.GetFiles(UpdatePath).Length > 0)
            {
                Application.Run(new FrmCopyFiles());
            }
            Process.Start("AccountManager.exe");
        }
        public static readonly string UpdatePath = "UpdateFiles";
    }
}